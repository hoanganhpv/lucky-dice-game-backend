"use strict";

class UserFormData {
    constructor(userName, firstName, lastName) {
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
    };
};

let gNewUserFormData = null;

let gResultArr = [];

const gGetDiceBtn = document.querySelector('#dice-btn');

gGetDiceBtn.addEventListener("click", () => {
    onGetUserData();
    if (onValidate(gNewUserFormData)) {
        onCallApiToPostData(gNewUserFormData);
    };
});

const gGetDiceHistory = document.querySelector('#btn-get-dice-history');

gGetDiceHistory.addEventListener('click', () => {
    onGetUserData();
    const vTableHead = document.querySelector('#table-head');
    const vTableBody = document.querySelector('#table-body');
    // vTableHead.removeChild(vTableHead.childNodes[0]);
    // vTableBody.removeChild(vTableBody.childNodes[0]);
    vTableHead.innerHTML = '';
    vTableBody.innerHTML = '';
    if (onValidate(gNewUserFormData)) {
        onCallApiToGetDiceHistory(gNewUserFormData);
    };
    if (gResultArr.length > 0) {
        const vTableHeadingRow = document.createElement('tr');
        const vTableHeading1 = document.createElement('th');
        const vTableHeading2 = document.createElement('th');
        vTableHeading1.innerHTML = 'Lượt Ném!';
        vTableHeading2.innerHTML = 'Số điểm!';
        vTableHeading1.setAttribute('class', 'col-sm-9');
        vTableHeading1.setAttribute('class', 'text-center');
        vTableHeading2.setAttribute('class', 'col-sm-3');
        vTableHeading2.setAttribute('class', 'text-center');
        vTableHeadingRow.appendChild(vTableHeading1);
        vTableHeadingRow.appendChild(vTableHeading2);
        vTableHead.appendChild(vTableHeadingRow);

        for (let i = 0; i < gResultArr.length; i++) {
            const vTableBodyRow = document.createElement('tr');
            const vTableDataOrder = document.createElement('td');
            const vTableDataDice = document.createElement('td');
            vTableDataOrder.innerHTML = i + 1;
            vTableDataDice.innerHTML = gResultArr[i];
            vTableBodyRow.appendChild(vTableDataOrder);
            vTableBodyRow.appendChild(vTableDataDice);
            vTableBodyRow.setAttribute('class', 'col-sm-12');
            vTableDataOrder.setAttribute('class', 'col-sm-9');
            vTableDataOrder.setAttribute('class', 'text-center');
            vTableDataDice.setAttribute('class', 'col-sm-3');
            vTableDataDice.setAttribute('class', 'text-center');
            vTableBody.appendChild(vTableBodyRow);
        };
    };
});

function onGetUserData() { //Lấy dữ liệu người dùng nhập bỏ vào đối tượng
    const vUsernameVal = document.querySelector('#user-name').value;
    const vFirstnameVal = document.querySelector('#first-name').value;
    const vLastnameVal = document.querySelector('#last-name').value;

    gNewUserFormData = new UserFormData(vUsernameVal, vFirstnameVal, vLastnameVal);
};

function onValidate(paramUserDataObj) {
    if (paramUserDataObj['userName'] == '') {
        alert('Hãy nhập username!');
        return false;
    } else if (paramUserDataObj['firstName'] == '') {
        alert('Hãy nhập firstname!');
        return false;
    } else if (paramUserDataObj['lastName'] == '') {
        alert('Hãy nhập lastname!');
        return false;
    } else {
        return true;
    };
};

function onCallApiToPostData(paramUserDataObj) {
    let vXhrPostData = new XMLHttpRequest;
    vXhrPostData.open('POST', 'http://203.171.20.210:8080/devcamp-lucky-dice//dice', true);
    vXhrPostData.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
    vXhrPostData.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log('Đã gửi dữ liệu thành công!');
            console.log(vXhrPostData.responseText)
            let vNewVoucheObjResponse = JSON.parse(vXhrPostData.responseText);
            gResultArr.push(vNewVoucheObjResponse['dice']);
            onShowResult(vNewVoucheObjResponse);
            console.log(gResultArr);
        };
    };
    vXhrPostData.send(JSON.stringify(paramUserDataObj));
};

function onShowResult(paramNewVoucherObj) {
    let vImgResultSrc = document.querySelector('#dice-img-result');
    let vTitleResult = document.querySelector('#dice-title-result');
    let vidResult = document.querySelector('#id-response');
    let vDiscountResult = document.querySelector('#discount-response');
    switch (paramNewVoucherObj['dice']) {
        case 1: {
            vImgResultSrc.setAttribute('src', './LuckyDiceImages/1.png');
            vTitleResult.innerHTML = `<b>Gà!</b>`;
            break;
        };
        case 2: {
            vImgResultSrc.setAttribute('src', './LuckyDiceImages/2.png');
            vTitleResult.innerHTML = `<b>Gà!</b>`;
            break;
        };
        case 3: {
            vImgResultSrc.setAttribute('src', './LuckyDiceImages/3.png');
            vTitleResult.innerHTML = `<b>Gà!</b>`;
            break;
        };
        case 4: {
            vImgResultSrc.setAttribute('src', './LuckyDiceImages/4.png');
            vTitleResult.innerHTML = `<b>Cũng được!</b>`;
            break;
        };
        case 5: {
            vImgResultSrc.setAttribute('src', './LuckyDiceImages/5.png');
            vTitleResult.innerHTML = `<b>Có cố gắng!</b>`;
            break;
        };
        case 6: {
            vImgResultSrc.setAttribute('src', './LuckyDiceImages/6.png');
            vTitleResult.innerHTML = `<b>Tuyệt vời!</b>`;
            break;
        };
    };
    if (paramNewVoucherObj['voucher'] != null) {
        vidResult.innerHTML = paramNewVoucherObj['voucher']['id'];
        vDiscountResult.innerHTML = paramNewVoucherObj['voucher']['phanTramGiamGia'] + ' %';
    }
};

function onCallApiToGetDiceHistory(paramUserDataObj) {
    let vXhrDiceHistory = new XMLHttpRequest;
    vXhrDiceHistory.open('GET', `http://203.171.20.210:8080/devcamp-lucky-dice/prize-history?username=${paramUserDataObj['userName']}`, true);
    vXhrDiceHistory.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(vXhrDiceHistory.responseText);
        };
    };
    vXhrDiceHistory.send();
};