const createAVoucherHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for create a Voucherhistory...`);
    next();
};

const getAllVoucherHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for get all Voucherhistory...`);
    next();
};

const getAVoucherHistoryByIdMiddleware = (req, res, next) => {
    console.log(`server is preparing for get a Voucherhistory by ID...`);
    next();
};

const updateAVoucherHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for update a Voucherhistory...`);
    next();
};

const deleteAVoucherHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for delete a Voucherhistory...`);
    next();
};


export {
    createAVoucherHistoryMiddleware,
    getAllVoucherHistoryMiddleware,
    getAVoucherHistoryByIdMiddleware,
    updateAVoucherHistoryMiddleware,
    deleteAVoucherHistoryMiddleware
}