const createAPrizeHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for create a Prizehistory...`);
    next();
};

const getAllPrizeHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for get all Prizehistory...`);
    next();
};

const getAPrizeHistoryByIdMiddleware = (req, res, next) => {
    console.log(`server is preparing for get a Prizehistory by ID...`);
    next();
};

const updateAPrizeHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for update a Prizehistory...`);
    next();
};

const deleteAPrizeHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for delete a Prizehistory...`);
    next();
};


export {
    createAPrizeHistoryMiddleware,
    getAllPrizeHistoryMiddleware,
    getAPrizeHistoryByIdMiddleware,
    updateAPrizeHistoryMiddleware,
    deleteAPrizeHistoryMiddleware
}