const createADiceHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for create a dicehistory...`);
    next();
};

const getAllDiceHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for get all dicehistory...`);
    next();
};

const getADiceHistoryByIdMiddleware = (req, res, next) => {
    console.log(`server is preparing for get a dicehistory by ID...`);
    next();
};

const updateADiceHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for update a dicehistory...`);
    next();
};

const deleteADiceHistoryMiddleware = (req, res, next) => {
    console.log(`server is preparing for delete a dicehistory...`);
    next();
};


export {
    createADiceHistoryMiddleware,
    getAllDiceHistoryMiddleware,
    getADiceHistoryByIdMiddleware,
    updateADiceHistoryMiddleware,
    deleteADiceHistoryMiddleware
}