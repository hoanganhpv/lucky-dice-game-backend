const createAPrizeMiddleware = (req, res, next) => {
    console.log(`server is preparing for create a Prize...`);
    next();
};

const getAllPrizeMiddleware = (req, res, next) => {
    console.log(`server is preparing for get all Prize...`);
    next();
};

const getAPrizeByIdMiddleware = (req, res, next) => {
    console.log(`server is preparing for get a Prize by ID...`);
    next();
};

const updateAPrizeMiddleware = (req, res, next) => {
    console.log(`server is preparing for update a Prize...`);
    next();
};

const deleteAPrizeMiddleware = (req, res, next) => {
    console.log(`server is preparing for delete a Prize...`);
    next();
};

export {
    createAPrizeMiddleware,
    getAllPrizeMiddleware,
    getAPrizeByIdMiddleware,
    updateAPrizeMiddleware,
    deleteAPrizeMiddleware
}