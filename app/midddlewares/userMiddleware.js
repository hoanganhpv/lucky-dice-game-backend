const createAUserMiddleware = (req, res, next) => {
    console.log(`prepare for create a user in usermiddleware...`);
    next();
};

const getAllUserMiddlewareMiddleware = (req, res, next) => {
    console.log(`prepare for get all user and send to client UI...`);
    next();
};

const getAUserByIdMiddleware = (req, res, next) => {
    console.log(`prepare for get a user by id...`);
    next();
};

const updateAUserByIdMiddleware = (req, res, next) => {
    console.log(`prepare for update a user...`);
    next();
};

const deleteAUserMiddleware = (req, res, next) => {
    console.log(`Server is preparing for delete a user...`);
    next();
};


export {
    createAUserMiddleware,
    getAllUserMiddlewareMiddleware,
    getAUserByIdMiddleware,
    updateAUserByIdMiddleware,
    deleteAUserMiddleware
}