const createAVoucherMiddleware = (req, res, next) => {
    console.log(`server is preparing for create a Voucher...`);
    next();
};

const getAllVoucherMiddleware = (req, res, next) => {
    console.log(`server is preparing for get all Voucher...`);
    next();
};

const getAVoucherByIdMiddleware = (req, res, next) => {
    console.log(`server is preparing for get a Voucher by ID...`);
    next();
};

const updateAVoucherMiddleware = (req, res, next) => {
    console.log(`server is preparing for update a Voucher...`);
    next();
};

const deleteAVoucherMiddleware = (req, res, next) => {
    console.log(`server is preparing for delete a Voucher...`);
    next();
};

export {
    createAVoucherMiddleware,
    getAllVoucherMiddleware,
    getAVoucherByIdMiddleware,
    updateAVoucherMiddleware,
    deleteAVoucherMiddleware
}