import mongoose from "mongoose";

const Schema = mongoose.Schema;

const voucherSchema = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        unique: true
    },
    code: {
        type: String,
        unique: true,
        require: true
    },
    discount: {
        type: Number,
        require: true
    },
    note: {
        type: String,
        require: false
    }
}, {
    timestamps: true
});

const Voucher = mongoose.model('Voucher', voucherSchema);

export { Voucher };