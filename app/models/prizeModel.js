import mongoose from "mongoose";

const Schema = mongoose.Schema;

const prizeSchema = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        unique: true
    },
    name: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: false
    }
}, {
    timestamps: true
})

const Prize = mongoose.model('Prize', prizeSchema);

export { Prize };