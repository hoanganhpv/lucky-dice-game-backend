import mongoose from "mongoose";
import shortid from "shortid";

const Schema = mongoose.Schema;

const userSchema = Schema({
    _id: {
        type: Schema.Types.ObjectId,
        unique: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    firstname: {
        type: String,
        require: true
    },

    lastname: {
        type: String,
        require: true
    },
    dices: [{
        type: String,
        ref: 'DiceHistory'
    }]
}, {
    timestamps: true
});

const User = mongoose.model('User', userSchema);

export { User };