import mongoose from "mongoose";

const Schema = mongoose.Schema;

const prizeHistorySchema = Schema({
    _id: Schema.Types.ObjectId,
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        require: true
    },
    prize: {
        type: Schema.Types.ObjectId,
        ref: 'Prize',
        require: true
    }
}, {
    timestamps: true
});

const PrizeHistory = mongoose.model('PrizeHistory', prizeHistorySchema);

export { PrizeHistory };