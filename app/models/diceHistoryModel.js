import mongoose from "mongoose";
import shortid from "shortid";

const Schema = mongoose.Schema;

const diceHistoryModelSchema = Schema({
    _id: {
        type: String,
        default: shortid.generate,
        unique: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        require: true
    },
    dice: {
        type: Number,
        require: true
    }
}, {
    timestamps: true
});

const DiceHistory = mongoose.model('DiceHistory', diceHistoryModelSchema);

export { DiceHistory };