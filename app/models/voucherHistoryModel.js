import mongoose, { modelNames } from "mongoose";

const Schema = mongoose.Schema;

const voucherHistorySchema = Schema({
    _id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        require: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: 'Voucher',
        require: true
    }
}, {
    timestamps: true
});

const VoucherHistory = mongoose.model('VoucherHistory', voucherHistorySchema);

export { VoucherHistory };