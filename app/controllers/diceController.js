import mongoose from "mongoose";

import { DiceHistory } from '../models/diceHistoryModel.js';
import { User } from '../models/userModel.js';
import { VoucherHistory } from '../models/voucherHistoryModel.js'


const getRandomVOucherController = async (req, res) => {
    const session = await startSession();
    await session.startTransaction();
    try {
        const { firstname, lastname, username } = req.body;

        if (!firstname || !lastname || !username) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        if (typeof (firstname) !== 'string' || typeof (lastname) !== 'string' || typeof (username) !== 'string') {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const dice = Math.floor(Math.random() * 6 + 1);

        const vFindUserResult = await User.findOne({
            username: username
        });

        if (!vFindUserResult) {
            const vNewUser = new User({
                _id: new mongoose.Types.ObjectId(),
                username: username,
                firstname: firstname,
                lastname: lastname
            });

            const vNewUserValidation = await vNewUser.validateSync();

            if (vNewUserValidation) {
                return res.status(400).json({
                    error: `Bad request!`
                });
            };
            const vCreateNewUserResult = await User.create();

            if (!vCreateNewUserResult) {
                return res.status(500).json({
                    error: `Internal server error!`
                });
            };

            const vNewDiceHistory = new DiceHistory({
                _id: new mongoose.Types.ObjectId(),
                user: vNewUser._id,
                dice: dice
            });

            const vNewDiceHistoryValidation = vNewDiceHistory.validateSync();

            if(vNewDiceHistoryValidation) {
                return res.status(400).json({
                    error: `Bad request!`
                });
            };

            const vCreateNewDiceHistoryResult = await DiceHistory.create(vNewDiceHistory);

            if(vCreateNewDiceHistoryResult) {
                return res.status(500).json({
                    error: `Internal server error!`
                });
            };

            



        };





    } catch (err) {

    } finally {
        session.endSession();
    }
}