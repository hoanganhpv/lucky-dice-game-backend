import { User } from '../models/userModel.js';

import mongoose, { startSession } from 'mongoose';

const createAUserController = async (req, res) => {
    try {
        console.log("hello!")
        const { username, firstname, lastname } = req.body;
        console.log(username);
        console.log(firstname);
        console.log(lastname);
        if (!username || typeof (username) !== 'string') {
            return res.status(400).json({
                errorMessage: `bad request!`
            });
        };

        if (!firstname || typeof (firstname) !== 'string') {
            return res.status(400).json({
                errorMessage: `bad request!`
            });
        };

        if (!lastname || typeof (lastname) !== 'string') {
            return res.status(400).json({
                errorMessage: `bad request!`
            });
        };

        const vNewUser = new User({
            _id: new mongoose.Types.ObjectId(),
            username: username,
            firstname: firstname,
            lastname: lastname
        });

        const vNewUserValidation = await vNewUser.validateSync();

        if (vNewUserValidation) {
            return res.status(500).json({
                errorMessage: `Internal server error!`
            });
        };

        const vExistedUsernameResult = await User.findOne({username: username});

        if(vExistedUsernameResult) {
            return res.status(409).json({
                error: `this username has already exist!`
            })
        }

        const vCreateResult = await User.create(vNewUser);

        if(!vCreateResult) {
            return res.status(409).json({
                error: `Conflig!`
            });
        };

        res.status(201).json({
            message: `Create successfully!`
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const getAllUserMiddlewareController = async (req, res) => {
    try {
        const getAllUserResult = await User.find();

        res.status(200).json({
            message: `Get all user OK!`,
            result: getAllUserResult
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const getAUserByIdController = async (req, res) => {
    try {
        const vUserId = req.params.userId;
        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vGetAUserByIdResult = await User.findById(vUserId);

        if (!vGetAUserByIdResult) {
            return res.status(404).json({
                errorMessage: `Not found!`
            });
        } else {
            res.status(200).json({
                result: vGetAUserByIdResult
            });
        };
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

const updateAUserByIdController = async (req, res) => {
    try {
        const vUserId = req.params.userId;
        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const { username, firstname, lastname } = req.body;

        if (username && typeof (username) !== 'string') {
            return res.status(400).json({
                errorMessage: `bad request!`
            });
        };

        if (firstname && typeof (firstname) !== 'string') {
            return res.status(400).json({
                errorMessage: `bad request!`
            });
        };

        if (lastname && typeof (lastname) !== 'string') {
            return res.status(400).json({
                errorMessage: `bad request!`
            });
        };

        const vNewUserData = {
            username: username,
            firstname: firstname,
            lastname: lastname
        };

        const vUpdateResult = await User.findByIdAndUpdate(vUserId, vNewUserData);

        if (!vUpdateResult) {
            return res.status(404).json({
                errorMessage: `404 Not found!`
            });
        } else {
            console.log(`Update successfully!`);
            res.status(204).json({
                result: vUpdateResult
            });
        };
    } catch (err) {
        console.log(err);
        res.status(500).json('Internal server error!')
    };
};

const deleleAUserController = async (req, res) => {
    try {
        const vUserId = req.params.userId;

        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vDeleteUserResult = await User.findOneAndDelete({
            _id: vUserId
        }, {
            new: true
        });

        if (!vDeleteUserResult) {
            return res.status(404).json({
                errorMessage: `404 Not found!`
            });
        } else {
            console.log(`delete a user successfully!`);
            res.status(204).json(`delete done!`)
        };
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

export {
    createAUserController,
    getAllUserMiddlewareController,
    getAUserByIdController,
    updateAUserByIdController,
    deleleAUserController
};