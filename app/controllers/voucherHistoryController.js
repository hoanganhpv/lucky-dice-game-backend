import mongoose, { modelNames } from "mongoose";

import { VoucherHistory } from "../models/voucherHistoryModel.js";

const createAVoucherHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const { user, voucher } = req.body;

        if (!user || typeof (user) !== 'string') {
            return res.status(400).json({
                err: `bad request!`
            });
        };

        if (!voucher || typeof (voucher) !== 'string') {
            return res.status(400).json({
                err: `bad request!`
            });
        };

        const vNewVoucherHistory = new VoucherHistory({
            _id: new mongoose.Types.ObjectId(),
            user: new mongoose.Types.ObjectId(user),
            prize: new mongoose.Types.ObjectId(voucher)
        });

        const vNewVoucherHistoryValidation = await vNewVoucherHistory.validateSync();
        if (vNewVoucherHistoryValidation) {
            return res.status(400).json({
                errorMessage: `Bad request!`,
                details: vNewVoucherHistoryValidation.errors
            });
        };

        const vCreateNewVoucherHistoryResult = await vNewVoucherHistory.save();

        if (!vCreateNewVoucherHistoryResult) {
            return res.status(404).json({
                errorMessage: `Bad request!`
            });
        };

        res.status(201).json({
            result: vCreateNewVoucherHistoryResult
        });

        session.commitTransaction();
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
        session.abortTransaction();
    } finally {
        session.endSession();
    };
};

const getAllVoucherHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
        const vUser = req.query.user;
        let condition = {};

        if (vUser) {
            condition.user = vUser
        };
        
        const vGetAllDicesResult = await VoucherHistory.find(condition);
        if (!vGetAllDicesResult) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };
        
        res.status(200).json({
            result: vGetAllDicesResult
        });
        session.commitTransaction();
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    } finally {
        session.endSession();
    }
};

const getAVoucherHistoryByIdController = async (req, res) => {
    const session = mongoose.startSession();
    (await session).startTransaction();
    try {
        const vVoucherHistoryId = req.params.voucherHistoryId;
        if (!mongoose.Types.ObjectId.isValid(vVoucherHistoryId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vGetAVoucherHistoryResult = await VoucherHistory.findById(vVoucherHistoryId);
        if (!vGetAVoucherHistoryResult) {
            return res.status(404).json({
                errorMessage: `Bad request!, 2`
            });
        } else {
            (await session).commitTransaction();
            res.status(200).json({
                result: vGetAVoucherHistoryResult
            });
        };
    } catch (err) {
        (await session).abortTransaction();
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        })
    } finally {
        (await session).endSession();
    }
};

const updateAVoucherHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const vVoucherHistoryId = req.params.voucherHistoryId;

        if (!mongoose.Types.ObjectId.isValid(vVoucherHistoryId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const { user, voucher } = req.body;

        if (user && !mongoose.Types.ObjectId.isValid(user)) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        if (voucher && !mongoose.Types.ObjectId.isValid(voucher)) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const vUpdateVoucherHistory = await VoucherHistory.findOneAndUpdate({
            _id: vVoucherHistoryId
        }, {
            $set: req.body
        }, {
            new: true,
            omitUndefined: true
        });

        if (!vUpdateVoucherHistory) {
            return res.status(404).json({
                errorMessage: `error!`
            });
        };

        console.log(`Update successfully!`);
        res.status(204).json({
            result: vUpdateVoucherHistory
        });
        session.commitTransaction();

    } catch (err) {
        session.abortTransaction();
        console.log(err);
        errorMessage: `Internal erver error!`
    } finally {
        session.endSession();
    };
};

const deleteAVoucherHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const vVoucherHistoryId = req.params.voucherHistoryId;

        if (!mongoose.Types.ObjectId.isValid(vVoucherHistoryId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vDeleteVoucherHistoryResult = await VoucherHistory.findOneAndDelete({
            _id: vVoucherHistoryId
        }, {
            new: true
        });

        if (!vDeleteVoucherHistoryResult) {
            return res.status(404).json({
                errorMessage: `error, 404 not found!`
            });
        };

        console.log(`Delete Successfully!`)
        res.status(204).json({
            result: `Delele successfully!`
        });

    } catch (err) {
        {
            session.abortTransaction();
            console.log(err);
            res.status(500).json({
                errorMessage: `Ìnternal sever error!`
            })
        }
    } finally {
        session.endSession();
    };
};

export {
    createAVoucherHistoryController,
    getAllVoucherHistoryController,
    getAVoucherHistoryByIdController,
    updateAVoucherHistoryController,
    deleteAVoucherHistoryController
};