import mongoose, { startSession } from 'mongoose';
import { Voucher } from '../models/voucherModel.js';

const createAVoucherController = async (req, res) => {
    const session = await startSession();
    await session.startTransaction();
    try {
        const { code, discount, note } = req.body;

        if (!code || typeof (code) !== 'string') {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        if (!discount || typeof (discount) !== 'number') {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        if (note && typeof (note) !== 'string') {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const vNewVoucher = new Voucher({
            _id: new mongoose.Types.ObjectId(),
            code: code,
            discount: discount,
            note: note
        });

        const vNewVoucherValidation = await vNewVoucher.validateSync();

        if (vNewVoucherValidation) {
            return res.status(500).json({
                error: `Internal server error!`
            });
        };

        console.log(`Creating`)

        const vCreateAVoucherResult = await Voucher.create(vNewVoucher);

        if (!vCreateAVoucherResult) {
            return res.status(500).json({
                error: `Internal server error!`
            });
        };

        res.status(201).json({
            result: vCreateAVoucherResult
        });

        session.commitTransaction();

    } catch (err) {
        session.abortTransaction();
        console.log(err);
        res.status(500).json({
            error: `Internal server error!`
        });
    } finally {
        session.endSession();
    };
};

const getAllVoucherController = async (req, res) => {
    try {
        const vGetAllVoucherResult = await Voucher.find().sort({
            discount: 'asc'
        });

        if (!vGetAllVoucherResult) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        res.status(200).json({
            result: vGetAllVoucherResult
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            err: `Internal server error!`
        })
    } finally {
        console.log(`done getALlVoucher function!`)
    }
}

const getAVoucherByIdController = async (req, res) => {
    try {
        const vVoucherId = req.params.voucherId;

        if (!mongoose.Types.ObjectId.isValid(vVoucherId)) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const vGetAVoucherByIdResult = await Voucher.findById(vVoucherId);

        if (!vGetAVoucherByIdResult) {
            return res.status(500).json({
                err: `Internal server  error!`
            });
        };

        res.status(200).json({
            result: vGetAVoucherByIdResult
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            err: `Internal server error!s`
        });
    } finally {
        console.log(`done function!`)
    };
};

const updateAVoucherController = async (req, res) => {
    const session = await startSession();
    session.startTransaction();
    try {
        const vVoucherId = req.params.voucherId;

        if (!vVoucherId) {
            return res.status(400).json({
                err: `Bad request!`
            })
        };

        const { code, discount, note } = req.body;

        if (discount && typeof (discount) !== 'number') {
            return res.status(400).json({
                err: `Bad request!`
            });
        };

        if (code && typeof (code) !== 'string') {
            return res.status(400).json({
                err: `Bad request!`
            });
        };

        if (note && typeof (note) !== 'string') {
            return res.status(400).json({
                err: `Bad request!`
            });
        };

        if (!code && !discount && !note) {
            return res.status(400).json({
                err: `Bad request!`
            });
        };

        const vUpdateAVoucherResult = await Voucher.findOneAndUpdate({ _id: vVoucherId }, { $set: req.body }, { new: true, omitUndefined: true });

        if (!vUpdateAVoucherResult) {
            return res.status(500).json({
                err: `Internal sevrer Error!`
            });
        };

        console.log(`Update OK!`)

        session.commitTransaction();

        res.status(204).json({
            result: `OK!`
        })


    } catch (err) {
        session.endSession();
        console.log(err);
        res.status(500).json({
            error: `Internal server error!`
        });
    } finally {
        session.endSession();
        console.log(`done update Voucher function!`)
    };
};

const deleleAVoucherController = async (req, res) => {
    const session = startSession();
    await (await session).startTransaction();
    try {
        const vVoucherId = req.params.voucherId;

        if (!mongoose.Types.ObjectId.isValid(vVoucherId)) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const vDeleteAVoucherResult = await Voucher.findOneAndDelete({
            _id: vVoucherId
        });

        if (!vDeleteAVoucherResult) {
            return res.status(500).json({
                error: `Internal server error!`
            });
        };

        (await session).commitTransaction();

        console.log(`Delete Success!`)

        res.status(204).json({
            result: `OK`
        })

    } catch (err) {
        (await session).abortTransaction();
        console.log(err);
        res.status(500).json({
            err: `Internal server error!`
        })
    } finally {
        (await session).endSession();
        console.log(`done delete Voucher function!`)
    }
}

export {
    createAVoucherController,
    getAVoucherByIdController,
    getAllVoucherController,
    updateAVoucherController,
    deleleAVoucherController
};