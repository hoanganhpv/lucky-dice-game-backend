import mongoose from "mongoose";
import shortid from "shortid";

import { DiceHistory } from "../models/diceHistoryModel.js";
import { User } from "../models/userModel.js";

const createADiceHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const { dice } = req.body;

        const vUserId = req.params.userId;

        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const vExistedUserStatus = await User.findOne({_id: vUserId});

        if(!vExistedUserStatus) {
            return res.status(400).json({
                err: `User Id not found!`
            });
        };

        const vNewDice = new DiceHistory({
            user: new mongoose.Types.ObjectId(vUserId),
            dice: dice
        });

        if (!dice || typeof (dice) !== 'number' || dice < 0 || dice > 6) {
            return res.status(400).json({
                errorMessage: `Bad request!`,
            });
        };

        const vNewDiceValidation = await vNewDice.validateSync();
        if (vNewDiceValidation) {
            return res.status(400).json({
                errorMessage: `Bad request!`,
                details: vNewDiceValidation.errors
            });
        };

        const vCreateNewDiceResult = await vNewDice.save();

        if (!vCreateNewDiceResult) {
            return res.status(404).json({
                errorMessage: `Bad request!`
            });
        };

        const vAddDiceIdIntoUserResult = await User.findByIdAndUpdate(vUserId, {
            $push: {dices: vCreateNewDiceResult._id}
        });

        if(!vAddDiceIdIntoUserResult) {
            return res.status(500).json({
                error: `Internal server error!`
            });
        };

        res.status(201).json({
            result: vCreateNewDiceResult
        });

        session.commitTransaction();
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
        session.abortTransaction();
    } finally {
        session.endSession();
    };
};

const getAllDiceHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
        const vUser = req.query.user;

        let condition = {};

        if (vUser) {
            condition.user = vUser
        };

        const vGetAllDicesResult = await DiceHistory.find(condition);

        if (!vGetAllDicesResult) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        res.status(200).json({
            result: vGetAllDicesResult
        });
        session.commitTransaction();
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    } finally {
        session.endSession();
    }
};

const getADiceHistoryByIdController = async (req, res) => {
    const session = mongoose.startSession();
    (await session).startTransaction();
    try {
        const vUserId = req.params.userId;
        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Badrequest!`
            });
        };

        const vCurrExistedUserResult = await User.findOne({_id: vUserId});

        if(!vCurrExistedUserResult) {
            return res.status(400).json({
                err: `User not found!`
            });
        };

        const vGetADiceResult = await User.findById(vUserId).populate('dices');
        if (!vGetADiceResult) {
            return res.status(404).json({
                errorMessage: `Bad request!`
            });
        } else {
            res.status(200).json({
                username: vGetADiceResult.username,
                user: vGetADiceResult.firstname + ' ' + vGetADiceResult.lastname,
                result: vGetADiceResult.dices
            });
        };
        (await session).commitTransaction();
    } catch (err) {
        (await session).abortTransaction();
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        })
    } finally {
        (await session).endSession();
    }
};

const updateADiceHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const vUserId = req.params.userId;
        const vDiceId = req.params.diceId;

        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vExistedDiceHistoryIdStatus = await DiceHistory.findOne({
            _id: vDiceId
        });

        if(!vExistedDiceHistoryIdStatus) {
            return res.status(404).json({
                err: `DiceId not found!`
            });
        };

        const { dice } = req.body;

        if (dice && typeof(dice) !== 'number') {
            return res.status(400).json({
                errorMessage: `Bad request -> dice not a Number!`
            });
        };

        const vUpdateDiceHistory = await DiceHistory.findOneAndUpdate({
            _id: vDiceId
        }, {
            $set: {dice: dice}
        }, {
            new: true,
            omitUndefined: true
        });

        if(!vUpdateDiceHistory) {
            return res.status(500).json({
                error: `Internal server error!`
            });
        };

        console.log(`Update successfully!`);
        res.status(204).json({
            result: vUpdateDiceHistory
        });
        session.commitTransaction();

    } catch (err) {
        session.abortTransaction();
        console.log(err);
        errorMessage: `Internal erver error!`
    } finally {
        session.endSession();
    };
};

const deleteADiceHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const vUserId = req.params.userId;
        const vDiceId = req.params.diceId;

        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vExistedUserStatus = await User.findOne({
            _id: vUserId
        });

        if(!vExistedUserStatus) {
            return res.status(404).json({
                error: `UserId not found!`
            });
        };

        const vExistedDiceHistoryIdStatus = await DiceHistory.findOne({
            _id: vDiceId
        });

        if (!vExistedDiceHistoryIdStatus) {
            return res.status(404).json({
                error: `DiceHistory not found!`
            });
        };

        const vDeleteDiceResult = await DiceHistory.findOneAndDelete({
            _id: vDiceId
        }, {
            new: true
        });

        if (!vDeleteDiceResult) {
            return res.status(404).json({
                errorMessage: `error, 404 not found!`
            });
        };

        console.log(`Delete Successfully!`)
        res.status(204).json({
            result: `Delele successfully!`
        });

    } catch (err) {
        {
            session.abortTransaction();
            console.log(err);
            res.status(500).json({
                errorMessage: `Ìnternal sever error!`
            })
        }
    } finally {
        session.endSession();
    };
};

export {
    createADiceHistoryController,
    getAllDiceHistoryController,
    getADiceHistoryByIdController,
    updateADiceHistoryController,
    deleteADiceHistoryController
};