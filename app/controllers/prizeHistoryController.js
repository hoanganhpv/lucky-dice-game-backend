import mongoose, { modelNames } from "mongoose";

import { PrizeHistory } from "../models/prizeHistoryModel.js";
import { DiceHistory } from '../models/diceHistoryModel.js'

const createAprizeHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const { user, prize } = req.body;

        if (!user || typeof (user) !== 'string') {
            return res.status(400).json({
                err: `bad request!`
            });
        };

        if (!prize || typeof (prize) !== 'string') {
            return res.status(400).json({
                err: `bad request!`
            });
        };

        const vNewprizeHistory = new PrizeHistory({
            _id: new mongoose.Types.ObjectId(),
            user: new mongoose.Types.ObjectId(user),
            prize: new mongoose.Types.ObjectId(prize)
        });

        const vNewPrizeHistoryValidation = await vNewprizeHistory.validateSync();
        if (vNewPrizeHistoryValidation) {
            return res.status(400).json({
                errorMessage: `Bad request!`,
                details: vNewPrizeHistoryValidation.errors
            });
        };

        const vCreateNewPrizeHistoryResult = await vNewprizeHistory.save();

        if (!vCreateNewPrizeHistoryResult) {
            return res.status(404).json({
                errorMessage: `Bad request!`
            });
        };

        res.status(201).json({
            result: vCreateNewPrizeHistoryResult
        });

        session.commitTransaction();
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
        session.abortTransaction();
    } finally {
        session.endSession();
    };
};

const getAllprizeHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
        const vUser = req.query.user;
        
        const condition = {};
        
        if (vUser) {
            condition.user = vUser
        };
        
        const vGetAllDicesResult = await PrizeHistory.find(condition);

        if (!vGetAllDicesResult) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        res.status(200).json({
            result: vGetAllDicesResult
        });
        session.commitTransaction();
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    } finally {
        session.endSession();
    }
};

const getAprizeHistoryByIdController = async (req, res) => {
    const session = mongoose.startSession();
    (await session).startTransaction();
    try {
        const vPriceHistoryId = req.params.prizeId;
        if (!mongoose.Types.ObjectId.isValid(vPriceHistoryId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vGetAPrizeHistoryResult = await PrizeHistory.findById(vPriceHistoryId);
        if (!vGetAPrizeHistoryResult) {
            return res.status(404).json({
                errorMessage: `Bad request!, 2`
            });
        } else {
            (await session).commitTransaction();
            res.status(200).json({
                result: vGetAPrizeHistoryResult
            });
        };
    } catch (err) {
        (await session).abortTransaction();
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        })
    } finally {
        (await session).endSession();
    }
};

const updateAprizeHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const vPrizeId = req.params.prizeId;

        if (!mongoose.Types.ObjectId.isValid(vPrizeId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const { user, prize } = req.body;

        if (user && !mongoose.Types.ObjectId.isValid(user)) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        if (prize && !mongoose.Types.ObjectId.isValid(prize)) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const vUpdatePrizeHistory = await PrizeHistory.findOneAndUpdate({
            _id: vPrizeId
        }, {
            $set: req.body
        }, {
            new: true,
            omitUndefined: true
        });

        if (!vUpdatePrizeHistory) {
            return res.status(404).json({
                errorMessage: `error!`
            });
        };

        console.log(`Update successfully!`);
        res.status(204).json({
            result: vUpdatePrizeHistory
        });
        session.commitTransaction();

    } catch (err) {
        session.abortTransaction();
        console.log(err);
        errorMessage: `Internal erver error!`
    } finally {
        session.endSession();
    };
};

const deleteAprizeHistoryController = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const vPrizeId = req.params.prizeId;

        if (!mongoose.Types.ObjectId.isValid(vPrizeId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vDeletePrizeHistoryResult = await PrizeHistory.findOneAndDelete({
            _id: vPrizeId
        }, {
            new: true
        });

        if (!vDeletePrizeHistoryResult) {
            return res.status(404).json({
                errorMessage: `error, 404 not found!`
            });
        };

        console.log(`Delete Successfully!`)
        res.status(204).json({
            result: `Delele successfully!`
        });

    } catch (err) {
        {
            session.abortTransaction();
            console.log(err);
            res.status(500).json({
                errorMessage: `Ìnternal sever error!`
            })
        }
    } finally {
        session.endSession();
    };
};

export {
    createAprizeHistoryController,
    getAllprizeHistoryController,
    getAprizeHistoryByIdController,
    updateAprizeHistoryController,
    deleteAprizeHistoryController
};