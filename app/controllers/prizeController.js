import mongoose, { startSession } from 'mongoose';
import { Prize } from '../models/prizeModel.js';

const createAPrizeController = async (req, res) => {
    const session = await startSession();
    await session.startTransaction();
    try {
        const { name, description } = req.body;

        if (!name || typeof(name) !== 'string') {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        if (description && typeof(description) !== 'string') {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const vNewPrize = new Prize({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            description: description
        });

        const vNewPrizeValidation = await vNewPrize.validateSync();

        if(vNewPrizeValidation) {
            return res.status(500).json({
                error: `Internal server error!`
            });
        };

        console.log(`Creating`)

        const vCreateAPrizeResult = await Prize.create(vNewPrize);

        if(!vCreateAPrizeResult) {
            return res.status(500).json({
                error: `Internal server error!`
            });
        };

        res.status(201).json({
            result: vCreateAPrizeResult
        });

        session.commitTransaction();

    } catch (err) {
        session.abortTransaction();
        console.log(err);
        res.status(500).json({
            error: `Internal server error!`
        });
    } finally {
        session.endSession();
    };
};

const getAllprizeController = async (req, res) => {
    try {
        const vGetAllprizeResult = await Prize.find().sort({
            name: 'asc'
        });

        if(!vGetAllprizeResult) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        res.status(200).json({
            result: vGetAllprizeResult
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            err: `Internal server error!`
        })
    } finally {
        console.log(`done getALlPrize function!`)
    }
}

const getAPrizeByIdController = async (req, res) => {
    try {
        const vPrizeId = req.params.prizeId;

        if (!mongoose.Types.ObjectId.isValid(vPrizeId)) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const vGetAprizeByIdResult = await Prize.findById(vPrizeId);

        if(!vGetAprizeByIdResult) {
            return res.status(500).json({
                err: `Internal server  error!`
            });
        };

        res.status(200).json({
            result: vGetAprizeByIdResult
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            err: `Internal server error!s`
        });
    } finally {
        console.log(`done function!`)
    };
};

const updateAPrizeController = async (req, res) => {
    const session = await startSession();
    session.startTransaction();
    try {
        const vPrizeId = req.params.prizeId;

        if (!vPrizeId) {
            return res.status(400).json({
                err: `Bad request!`
            })
        };

        const { name, description } = req.body;

        if (name && typeof(name) !== 'string') {
            return res.status(400).json({
                err: `Bad request!`
            });
        };

        if (description && typeof(description) !== 'string') {
            return res.status(400).json({
                err: `Bad request!`
            });
        };

        if (!name && !description) {
            return res.status(400).json({
                err: `Bad request!`
            });
        };

        const vUpdateAPrizeResult = await Prize.findOneAndUpdate({_id: vPrizeId}, {$set: req.body}, {new: true, omitUndefined: true});

        if (!vUpdateAPrizeResult) {
            return res.status(500).json({
                err: `Internal sevrer Error!`
            });
        };

        console.log(`Update OK!`)

        session.commitTransaction();

        res.status(204).json({
            result: `OK!`
        })


    } catch (err) {
        session.endSession();
        console.log(err);
        res.status(500).json({
            error: `Internal server error!`
        });
    } finally {
        session.endSession();
        console.log(`done update prize function!`)
    };
};

const deleleAPrizeController = async (req, res) => {
    const session = startSession();
    await (await session).startTransaction();
    try {
        const vPrizeId = req.params.prizeId;

        if(!mongoose.Types.ObjectId.isValid(vPrizeId)) {
            return res.status(400).json({
                error: `Bad request!`
            });
        };

        const vDeleteAPrizeResult = await Prize.findOneAndDelete({
            _id: vPrizeId
        });

        if(!vDeleteAPrizeResult) {
            return res.status(500).json({
                error: `Internal server error!`
            });
        };

        (await session).commitTransaction();

        console.log(`Delete Success!`)

        res.status(204).json({
            result: `OK`
        })

    } catch (err) {
        (await session).abortTransaction();
        console.log(err);
        res.status(500).json({
            err: `Internal server error!`
        })
    } finally {
        (await session).endSession();
        console.log(`done delete prize function!`)
    }
}

export {
    createAPrizeController,
    getAPrizeByIdController,
    getAllprizeController,
    updateAPrizeController,
    deleleAPrizeController
};