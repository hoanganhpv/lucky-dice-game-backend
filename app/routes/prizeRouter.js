import express from 'express';
const prizeRouter = express.Router();
import {
    createAPrizeMiddleware,
    getAllPrizeMiddleware,
    getAPrizeByIdMiddleware,
    updateAPrizeMiddleware,
    deleteAPrizeMiddleware
} from '../midddlewares/prizeMiddleware.js';

import {
    createAPrizeController,
    getAPrizeByIdController,
    getAllprizeController,
    updateAPrizeController,
    deleleAPrizeController
} from '../controllers/prizeController.js';


prizeRouter.post('/prizes', createAPrizeMiddleware, createAPrizeController);

prizeRouter.get('/prizes/:prizeId', getAPrizeByIdMiddleware, getAPrizeByIdController);

prizeRouter.get('/prizes', getAllPrizeMiddleware, getAllprizeController);

prizeRouter.put('/prizes/:prizeId', updateAPrizeMiddleware, updateAPrizeController);

prizeRouter.delete('/prizes/:prizeId', deleteAPrizeMiddleware, deleleAPrizeController);

export { prizeRouter };