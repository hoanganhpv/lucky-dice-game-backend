import express from "express";

const voucherHistoryRouter = express.Router();

import {
    createAVoucherHistoryMiddleware,
    getAllVoucherHistoryMiddleware,
    getAVoucherHistoryByIdMiddleware,
    updateAVoucherHistoryMiddleware,
    deleteAVoucherHistoryMiddleware
} from '../midddlewares/voucherHistoryMiddleware.js';

import {
    createAVoucherHistoryController,
    getAllVoucherHistoryController,
    getAVoucherHistoryByIdController,
    updateAVoucherHistoryController,
    deleteAVoucherHistoryController
} from '../controllers/voucherHistoryController.js';

voucherHistoryRouter.post('/voucher-histories', createAVoucherHistoryMiddleware, createAVoucherHistoryController);

voucherHistoryRouter.get('/voucher-histories', getAllVoucherHistoryMiddleware, getAllVoucherHistoryController);

voucherHistoryRouter.get('/voucher-histories/:voucherHistoryId', getAVoucherHistoryByIdMiddleware, getAVoucherHistoryByIdController);

voucherHistoryRouter.put('/voucher-histories/:voucherHistoryId', updateAVoucherHistoryMiddleware, updateAVoucherHistoryController);

voucherHistoryRouter.delete('/voucher-histories/:voucherHistoryId', deleteAVoucherHistoryMiddleware, deleteAVoucherHistoryController)

export { voucherHistoryRouter };