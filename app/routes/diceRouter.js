import express from "express";

const diceRouter = express.Router();

import {
    getRandomVOucherMiddleware
} from '../midddlewares/diceMiddleware.js';

diceRouter.post('/devcamp-lucky-dice/dice', getRandomVOucherMiddleware);

export {
    diceRouter
}

