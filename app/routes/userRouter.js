import express from "express";

import {
    createAUserMiddleware,
    getAllUserMiddlewareMiddleware,
    getAUserByIdMiddleware,
    updateAUserByIdMiddleware,
    deleteAUserMiddleware,
} from '../midddlewares/userMiddleware.js';

import {
    createAUserController,
    getAllUserMiddlewareController,
    getAUserByIdController,
    updateAUserByIdController,
    deleleAUserController
} from '../controllers/userController.js'

const userRouter = express.Router();

userRouter.post('/users', createAUserMiddleware, createAUserController);

userRouter.get('/users', getAllUserMiddlewareMiddleware, getAllUserMiddlewareController);

userRouter.get('/users/:userId', getAUserByIdMiddleware, getAUserByIdController);

userRouter.put('/users/:userId', updateAUserByIdMiddleware, updateAUserByIdController);

userRouter.delete('/users/:userId', deleteAUserMiddleware, deleleAUserController);

export { userRouter };