import express from 'express';
const voucherRouter = express.Router();
import {
    createAVoucherMiddleware,
    getAllVoucherMiddleware,
    getAVoucherByIdMiddleware,
    updateAVoucherMiddleware,
    deleteAVoucherMiddleware
} from '../midddlewares/voucherMiddleware.js';

import {
    createAVoucherController,
    getAVoucherByIdController,
    getAllVoucherController,
    updateAVoucherController,
    deleleAVoucherController
} from '../controllers/voucherController.js';

voucherRouter.post('/vouchers', createAVoucherMiddleware, createAVoucherController);

voucherRouter.get('/vouchers/:voucherId', getAllVoucherMiddleware, getAVoucherByIdController);

voucherRouter.get('/vouchers', getAVoucherByIdMiddleware, getAllVoucherController);

voucherRouter.put('/vouchers/:voucherId', updateAVoucherMiddleware, updateAVoucherController);

voucherRouter.delete('/vouchers/:voucherId', deleteAVoucherMiddleware, deleleAVoucherController);

export { voucherRouter };