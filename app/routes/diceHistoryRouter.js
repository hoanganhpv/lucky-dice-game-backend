import express from "express";
const diceHistoryRouter = express.Router();

import {
    createADiceHistoryMiddleware,
    getAllDiceHistoryMiddleware,
    getADiceHistoryByIdMiddleware,
    updateADiceHistoryMiddleware,
    deleteADiceHistoryMiddleware
} from '../midddlewares/diceHistoryMiddleware.js';

import {
    createADiceHistoryController,
    getAllDiceHistoryController,
    getADiceHistoryByIdController,
    updateADiceHistoryController,
    deleteADiceHistoryController
} from '../controllers/diceHistoryController.js';

diceHistoryRouter.post('/users/:userId/dice-histories', createADiceHistoryMiddleware, createADiceHistoryController);

diceHistoryRouter.get('/dice-histories', getAllDiceHistoryMiddleware, getAllDiceHistoryController);

diceHistoryRouter.get('/users/:userId/dice-histories', getADiceHistoryByIdMiddleware, getADiceHistoryByIdController);

diceHistoryRouter.put('/users/:userId/dice-histories/:diceId', updateADiceHistoryMiddleware, updateADiceHistoryController);

diceHistoryRouter.delete('/users/:userId/dice-histories/:diceId', deleteADiceHistoryMiddleware, deleteADiceHistoryController)

export { diceHistoryRouter };