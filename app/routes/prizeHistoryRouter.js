import express from "express";

const prizeHistoryRouter = express.Router();

import {
    createAPrizeHistoryMiddleware,
    getAllPrizeHistoryMiddleware,
    getAPrizeHistoryByIdMiddleware,
    updateAPrizeHistoryMiddleware,
    deleteAPrizeHistoryMiddleware
} from '../midddlewares/prizeHistoryMiddleware.js';

import {
    createAprizeHistoryController,
    getAllprizeHistoryController,
    getAprizeHistoryByIdController,
    updateAprizeHistoryController,
    deleteAprizeHistoryController
} from '../controllers/prizeHistoryController.js';

prizeHistoryRouter.post('/prize-histories', createAPrizeHistoryMiddleware, createAprizeHistoryController);

prizeHistoryRouter.get('/prize-histories', getAllPrizeHistoryMiddleware, getAllprizeHistoryController);

prizeHistoryRouter.get('/prize-histories/:prizeId', getAPrizeHistoryByIdMiddleware, getAprizeHistoryByIdController);

prizeHistoryRouter.put('/prize-histories/:prizeId', updateAPrizeHistoryMiddleware, updateAprizeHistoryController);

prizeHistoryRouter.delete('/prize-histories/:prizeId', deleteAPrizeHistoryMiddleware, deleteAprizeHistoryController)

export { prizeHistoryRouter };