import express from 'express';
import mime from 'mime-types';
import path from 'path';
import { fileURLToPath } from 'url';
import morgan from 'morgan';

import mongoose from 'mongoose';
import { userRouter } from './app/routes/userRouter.js';
import { diceHistoryRouter } from './app/routes/diceHistoryRouter.js';
import { diceRouter } from './app/routes/diceRouter.js';
import { prizeRouter } from './app/routes/prizeRouter.js';
import { prizeHistoryRouter } from './app/routes/prizeHistoryRouter.js';
import { voucherRouter } from './app/routes/voucherRouter.js';
import { voucherHistoryRouter } from './app/routes/voucherHistoryRouter.js';

try {
    mongoose.connect('mongodb://127.0.0.1:27017/luckydice');
    console.log(`connect to database successfully!`);
} catch (err) {
    console.log(err);
};

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();

app.use(express.json());

const port = 8000;

app.get('/', (req, res) => {
    console.log(`Server listening clietn access at ${new Date()}`);
    const vFileLocal = path.join(`${__dirname}/app/views/index.html`);
    const vFileHeader = mime.lookup(vFileLocal);
    res.setHeader('Content-Type', vFileHeader);
    res.status(200).sendFile(vFileLocal);
});

app.use(express.static(path.join(`${__dirname}/app/views`), {
    setHeaders: (res, path, stat) => {
        res.set('Content-Type', mime.lookup(path));
    }
}));

app.use('/api', userRouter);
app.use('/api', diceHistoryRouter);
app.use('/api', prizeHistoryRouter);
app.use('/api', diceRouter);
app.use('/api', prizeRouter);
app.use('/api', voucherRouter);
app.use('/api', voucherHistoryRouter);

app.listen(port, () => {
    console.log(`Server alert: server is listening on port: ${port}`);
});